title: Testphase läuft! 
---
Die Testphase für die digitale Währung läuft! Mit der Testphase soll die Stabilität des Währungsservers über längere Zeit überprüft werden, indem die Server- und Clientapplikationen für die Währung schon jetzt zu Probezwecken zur Verfügung stehen.

### Management-Seiten
Mithilfe der Management-Seiten können Benutzeraccounts hinzugefügt, gelöscht und verändert werden. Zudem kann die Serverkonfiguration an sich verändert werden und es können Transaktionen erzwungen werden. Die Management-Seite ist ab sofort online.
Um Aktionen mit Administratorprivilegien durchzuführen, wird die Datei `master_cert` ("Master-Zertifikat") benötigt. Diese kann hier (Update: nicht mehr) zu Testzwecken heruntergeladen werden.

### Userterminal-Seite
Diese Website wird später auf den Notebooks für die Schüler laufen. Die QR-Code-Scanfunktion ist während der Testphase aus technischen Gründen deaktiviert.

*Alle jetzt eingegebenen Daten werden vor dem Projektbeginn gelöscht!*
**Bitte meldet alle Fehler an uns, z.B. über den GitHub Issue Tracker!**

##### [GitHub Repository](https://github.com/Jeija/schulealsstaat)

Anmerkung / Update:
Die Testphase wurde eingestellt, die Zentralbank bereitet sich jetzt intensiv auf einen mehrtägigen Probeeinsatz vor.
