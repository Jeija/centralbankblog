title: Das Zentralbankgesetz wurde verabschiedet
---
Die Projektleitung hat soeben das Gesetz zur Ordnung des Geldwesens nach §1.2.2.1. für gültig erklärt. Damit ist keine nachträgliche Änderung des Gesetzes durch Parlament, Gerichte oder sonstige Stellen mehr möglich.

Das Zentralbankgesetz findet ihr hier zum Download:
[... im PDF-Format](/blog/zentralbankgesetz.pdf)
[... im Markdown/Text-Format](/blog/zentralbankgesetz.txt)
