title: Zentralbank-Blog geht online
---
Diese Website steht nicht mit der "echten" europäischen Zentralbank in Verbindung - sie dient als Informationsplattform für das Schule-als-EU-Projekt am Hölderlin-Gymnasium in Nürtingen.

Diese Website befindet sich auf einem virtuellen Ubuntu-Server, der später als Proxy für Anfragen an das Währungssystem über das Internet dient.

Solange die HöGy-Zentralbank noch keine richtige Startseite mit den Downloads für die HöGyCoin-App hat, dient dieses Blog zudem als Startseite für die Domain [centralbank.eu](http://centralbank.eu/).
