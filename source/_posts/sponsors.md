title: Wir danken unseren Sponsoren!
---
Ein Großprojekt wie "Schule als EU" kommt nicht ohne Sponsoren aus - die Zentralbank freut sich über die Beiträge, die wir von unseren Sponsoren erhalten haben.


![eyeti-Logo](http://eyeti.de/wp-content/uploads/2014/07/eyetiLogoSmail462.png)
Die Firma [eyeti systems + solutions GmbH & Co. KG](http://eyeti.de) stellt der Zentralbank einen leistungsstarken Server, der die Anfragen der Smartphone-App und der Überweisungsterminals entgegennimmt. Das verbessert nicht nur die Geschwindigkeit der Überweisungen, sondern schafft auch mehr Stabilität und erlaubt uns, das System schon vor Projektbeginn ausgiebig zu testen. Vielen Dank an eyeti und an den Geschäftsführer Sven Noack für diese großzügige Hilfe! 

Zudem möchten wir uns bei Frau Geier-Baumann sowie bei Herrn Wolf und der Firma GIGATRONIK bedanken, die uns Notebooks zur Verfügung stellen. Diese Notebooks erlauben uns, Verwaltungsaufgaben schneller zu erledigen und mehr Geräte für Schule-als-EU-Unternehmen zur Verfügung zu stellen, die keine eigenen Laptops mitbringen konnten.
