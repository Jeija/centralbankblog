title: Karriere bei der Zentralbank
---
Ab sofort könnt ihr euch auf der [Karriereplattform](http://centralbank.eu/karriere) der Zentralbank über Stellenausschreibungen und Karrierechancen bei der HöGy-Zentralbank erkundigen.

Die Zentralbank bietet Arbeitsplätze im technischen und im administrativen Bereich. Damit habt ihr die Chance, besonders guten Einblick in die finanziellen und organisatorischen Hintergründe des Staates zu erhalten. Außerdem bieten die Stellen abwechslungsreiche Beschäftigungen, eine gute Entlohnung und Pausen / Freizeit zwischen den Arbeitsphasen.

Die Bewerbung erfolgt über eine Online-Plattform oder alternativ per E-Mail oder in Papierform.

Anmerkung / Update:
Wir freuen uns über die vielen Bewerbungen! Bitte beachtet, dass wir für den Währungswechsel und für die Eingangskontrolle keine weiteren Bewerber mehr annehmen können. Bei der Administration oder für den technischen Dienst können wir jeweils noch 1-2 weitere Personen einstellen.
